title: Ứng dụng Plex sẽ có mặt trên Apple TV
date: 2015-09-14 14:42:00 GMT+2
cover: /upload/092015-Plex.jpg
categories:
- Công nghệ
- Apple
tags:
- Flex
- Apple
- Apple TV
---

Với sự kiện Apple TV mới được công bố tuần trước cùng khả năng chạy các ứng dụng của bên thứ ba thông qua App Store của riêng mình, chúng ta có thể hình dung một số ứng dụng và dịch vụ tuyệt vời từ iOS sẽ được chuyển qua nền tảng này. Một trong đó đã được xác nhận là ứng dụng Plex, ứng dụng giải trí cho phép người dùng truyển tải và xem video trên các thiết bị di động của họ. Phát biểu với ITWORLD, đồng sáng lập PScott Olechowski cho biết họ đang háo hức thử nghiệm với phiên bản beta tvOS, hệ điều hành mới của Apple TV.

<!-- more -->

Ứng dụng Plex có mặt trên iOS khá lâu, tuy nhiên để có thể truyền tải nội dung lên Apple TV bạn phải có một thiết bị iOS để gửi lên thông qua AirPlay hoặc bẻ khoẻ Apple TV.

Plex cho biết đã từ lâu muốn mang ứng dụng của mình lên Apple TV và đây cũng là điều mà nhiều người dùng mong muốn. Mặc dù công ty đã xác nhận kế hoạch phát hành Plex cho Apple TV, vẫn chưa có ngày ra mắt cụ thể được nhắc đến.

Một điều may mắn là bạn sẽ không phải chờ lâu bởi theo như Olechowski đề cập Apple TV đang sử dụng Framework API giống như iOS. Do đó quá trình chuyển ứng dụng lên tvOS sẽ khá dễ dàng. Apple TV mới sẽ bắt đầu bán vào cuối tháng mười, Plex cho biết họ hy vọng ứng dụng sẽ ra mắt trong vòng vài tháng sau đó.

Nguồn: [ITWorld](http://www.itworld.com/article/2982961/hardware/exclusive-plex-is-coming-to-apple-tv.html)