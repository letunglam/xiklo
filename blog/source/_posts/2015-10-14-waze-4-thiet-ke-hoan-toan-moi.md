title: Waze sắp ra mắt phiên bản 4.0 với thiết kế hoàn toàn mới
date: 2015-10-14 12:00:00 GMT+2
cover: /upload/102015-waze.png
categories:
- Công nghệ
- Google
tags:
- Google
- Search
featured: true
---

Waze - công ty thuộc sở hữu của Google, một ứng dụng tra tìm đường đường có khả năng báo cáo sự cố giao thông đã công bố ra mắt phiên bản 4.0 sắp tới của ứng dụng.

<!-- more -->

Waze 4.0 đem đến thiết kế sáng sủa để dễ dàng tra tìm đường, thông báo tình trạng giao thông và chia sẻ. Các bước để hướng dẫn đường đi cũng đã được giảm xuống để thao tác nhanh hơn. Các bản đồ, trình đơn và thông báo tình trạng giao thông Waze hệ thống được thiết kế lại cho dễ nhìn và rõ ràng hơn, giảm lộn xộn thông tin trên bản đồ, các nút được thiết kế với màu sáng hơn. Waze đã được chỉnh sửa toàn diện tập trung mạnh mẽ vào người lái xe và trải nghiệm lái xe.

<div class="embed">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/NhKiJOX6zfo?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
