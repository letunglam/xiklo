title: Game trên Apple TV phải tương thích với Apple TV Remote
date: 2015-09-16 1:00:00 GMT+2
cover: /upload/092015-apple-tv-siri.png
categories:
- Công nghệ
- Apple
tags:
- Apple
- Apple TV
---

[Một lập trình viên](https://twitter.com/EvilBachus/status/643585682942771200) đã phát hiện ra rằng các trò chơi trên Apple TV đều phải tương thích với Apple TV remote, họ không thể đòi người dùng phải có thêm tay cầm MFi (tay cầm tương thích với iPhone, iPad). Qui định của Apple với lập trình viên cho tvOS có đề cập *"Trò chơi phải hỗ trợ Apple TV remote. Trò chơi không được yêu cầu phải có thêm tay cầm."* 

<!-- more -->

Điều này hoàn hợp lý khi đứng dưới góc độ trải nghiệm người sử dụng, họ không muốn có bất cứ ứng dụng trên App Store của Apple TV mà người dùng không thể tiếp cận được (vì thiếu tay cầm là một ví dụ). Nhưng nó sẽ có thể dẫn đến nhiều vấn đề. Một trong số đó là Apple TV Remote không có phím điều khiển  chuẩn để chơi game như các tay cầm MFi. 

Ngoài ra, nhiều trò chơi có trình điều khiển hết sức phức tạp với remote của Apple TV. Sẽ ra sao nếu Apple từ chối những trò chơi sử dụng Apple TV để thao tác những tính năng tối thiểu và yêu cầu người dùng sử dụng tay cầm để thực hiện các thao tác phức tạp hơn? Tệ nhất là một số lập trình viên có thể sẽ rời bỏ nền tảng Apple TV vì thấy rằng việc làm trò chơi tương thích với Apple TV remote là không đáng.

Tuy nhiên, một điều hết sức lạ là Apple mới cập nhật lại qui định, tại đó họ cho phép lập trình viên yêu cầu người dùng phải có tay cầm để chơi game. 

Trước khi quyết định mua Apple TV, chúng ta nên chờ xem diễn biến sắp tới sẽ ra sao. Đây có thể chỉ là một vấn đề nhỏ. Nhưng cũng có thể Apple sẽ lại thay đổi quan điểm. 

Nguồn:  [toucharcade](http://toucharcade.com/2015/09/15/apple-tv-games-must-be-playable-with-the-remote/)