title: iOS 9 chính thức ra mắt, watchOS 2 hoãn vì còn lỗi
date: 2015-09-16 03:00:00 GMT+2
cover: /upload/092015-ios-9.jpg
categories:
- Công nghệ
- Apple
tags:
- Apple
- iOS
- watchOS
- iPhone
- iPad
---

iOS 9 đã chính thức ra mắt hôm nay và iPhone, iPad của bạn có thể đã bắt đầu tải về (nếu bạn để tuỳ chọn tự động cập nhật). Trái ngược với iOS 9, watchOS thay vì ra mắt hôm nay như kì vọng thì sẽ bị hoãn lại vì vẫn còn lỗi. 

<!-- more -->

Theo phát ngôn viên của Apple đề cập, lỗi trên watchOS cần nhiều thời gian hơn để sửa và ngày ra mắt chưa xác định nhưng sẽ không để người dùng phải chờ quá lâu. 

Hệ điều hành mới bổ sung thêm một số tính năng mới, nhưng quan trọng nhất là cung cấp cho lập trình viên khả năng truy cập trực tiếp cảm biến và dữ liệu được giao tiếp hiệu quả hơn với iPhone. Các tính năng nổi bật khác của watchOS 2 là mở rộng hỗ trợ Siri, tìm đường, mặt đồng hồ, v.v...

Nguồn: [TechCrunch](http://techcrunch.com/2015/09/16/apple-delays-release-of-watchos-2-due-to-bug/)