title: Microsoft muốn Xbox 360 game chạy được trên PC
date: 2015-10-10 02:00:00 GMT+2
cover: /upload/102015-phil-spencer.jpg
categories:
- Công nghệ
- Microsoft
tags:
- Microsoft
- Xbox 360
- Xbox One
featured: true
---

Gần đây, Microsoft cho biết họ đang mang game Xbox 360 lên Xbox One. Một số tựa game Xbox 360 hiện đã chơi được trên Xbox One. Chưa dừng ở đó Microsoft đang lên kế hoạch mang game Xbox 360 lên cả hệ máy PC. 

<!-- more -->

Đây quả là một tin bất ngờ, theo lời của Phil Spencer, trưởng phòng Xbox phát biểu tại buổi phỏng vấn với Gizmodo Brazil:

> Xbox Game trên Xbox One là điều tôi muốn làm. Ở một vài khía cạnh tôi cũng thích việc có thể chơi game Xbox 360 trên hệ máy PC, vì thế chúng ta có nhiều thứ khác nhau phải nghĩ đến nếu lên kế hoạch. Nhưng sẽ rất tuyệt nếu có hỗ trợ Xbox game. Đúng vậy, tôi muốn thế, nhưng chúng tôi cũng muốn làm rất nhiều thứ khác nữa. 

Rõ ràng điều này không có nghĩa khả năng này sẽ đến sớm hoặc cũng có thể sẽ không đến, chúng ta không thể biết được. Nhưng thực sự, đây sẽ là điều hết sức thú vị nếu game thủ có thể chơi các trò chơi Xbox 360 trên PC của họ.

Nguồn: [Microsoft News](http://microsoft-news.com/xbox-boss-wants-xbox-360-games-to-work-on-the-pc/)