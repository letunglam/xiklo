title: Microsoft bắt đầu sử dụng trình đơn Start của Windows 10 để hiển thị quảng cáo
date: 2015-10-15 15:00:00 GMT+2
categories:
- Công nghệ
- Microsoft
tags:
- Microsoft
- Quảng cáo
- Windows
---

Quảng cáo là một phần của việc sử dụng internet - nhưng ngay cả trong Windows ư? Nếu bạn đã cập nhật bản 10565 của Windows 10, bạn sẽ thấy một bất ngờ: trình đơn Start bắt đầu được sử dụng để hiển thị quảng cáo.

![Windows 10 Start - quảng cáo](/upload/102015-start-ad.jpg)

<!-- more -->

Ở đây không nói về quảng cáo cho Viagra, khiêu dâm, hoặc bất cứ cái gì giống như thế mà là quảng cáo cho các ứng dụng. Tất nhiên, Microsoft không hiển thị dưới dạng quảng cáo; thay vào đó là 'Gợi ý ứng dụng' (Suggested apps). Đây là một tính năng hiện nay chỉ hiển thị cho người dùng thử nghiệm Windows, nhưng nó có thể sẽ được mang đến tất cả mọi người. Liệu nó sẽ được đón nhận?

Khó có thể hình dung rằng nhiều người sẽ chào đón sự xuất hiện của các quảng cáo này (dưới vỏ bọc 'gợi ý' hữu ích). Tuy nhiên, bạn có thể tắt tính năng nằng bằng cách bấm chuột phải vào 'Gợi ý ứng dụng' (Suggested apps) là bạn có thể chọn để ẩn  hoặc vô hiệu hóa tất cả các lời gợi ý trong tương lai.

Nguồn: [betanews](http://betanews.com/2015/10/15/microsoft-now-uses-windows-10s-start-menu-to-display-ads/l)