title: Apple thua kiện đại học Wisconsin, đối mặt với thiệt hại lên đến 862 triệu đô
date: 2015-10-14 10:00:00 GMT+2
categories:
- Công nghệ
- Apple
tags:
- Apple
- Patent
- iPhone
- iPad
---

Apple Inc có thể phải bồi thường thiệt hại lên đến 862 triệu đô sau khi ban bồi thẩm Mỹ vào thứ ba phát hiện công ty sử dụng công nghệ thuộc sở hữu của trường đại học Wisconsin-Madison trong các chip được tìm thấy ở nhiều chip iPhone và iPad mà không có được sự cho phép. 

Ban bồi thẩm tại Madison, Wisconsin cho biết bằng sáng chế của Hội nghiên cứu cựu sinh viên Wiconsin (Wisconsin Alumni Research Foundation - WARF) nhằm cải thiện hiệu quả xử lý, là hoàn toàn hợp lệ. Phiên tòa tiếp theo sẽ bắt đầu xác định Apple phải bồi thường thiệt hại bao nhiêu.

<!-- more -->

Apple đã từ chối bình luận về bản án. Trong một tuyên bố gửi qua email, WARF cho biết họ không bình luận về tranh chấp đang diễn ra.

WARF kiện Apple từ tháng 1 năm 2014, cáo buộc vi phạm các bằng sáng chế của họ năm 1998 trong việc cải thiện hiệu quả chip. Ban bồi thẩm đã xem xét xem liệu các bộ vi xử lý A7, A8 và A8X của Apple trong iPhone 5, 6 và 6 Plus, cũng như một số phiên bản của iPad có vi phạm bằng sáng chế trên hay không.

Văn phòng Apple tại Cupertino, California bác bỏ vi phạm với lập luận bằng sáng chế không hợp lệ dựa theo chứng từ toàn án. Trước đó, Apple đã cố gắng thuyết phục cơ quan sáng chế và nhãn hiệu Hoa Kỳ xem xét tính hợp lệ của bằng sáng chế, nhưng đã từ chối đề nghị của Apple trong tháng tư vừa qua.

Theo một phán quyết gần đây bởi Thẩm phán Hoa Kỳ William Conley, chủ trì vụ kiện, Apple có thể phải chịu trách nhiệm bồi thường thiệt hại lên đến 862.4 triệu đô. Ông dự kiến phiên tòa sẽ tiến hành trong ba giai đoạn: trách nhiệm pháp lý, thiệt hại và sau cùng tuỳ vào việc Apple có cố ý vi phạm bằng sáng chế hay không mà hình phạt sẽ được giữ nguyên hoặc tăng nặng.

WARF đã từng sử dụng bằng sáng chế trên để kiện Tập đoàn Intel vào năm 2008. Nhưng các đã được giải quyết vào năm sau ngay trước khi diễn ra phiên tòa. Vào tháng trước, WARF đã tiến hành vụ kiện thứ hai chống lại Apple, lần này mục tiêu là chip thế hệ mới nhất: A9 và A9X, chúng được sử dụng trong iPhone 6 và 6S mới được phát hành, cũng như iPad Pro.

Nguồn: [NBCNews](http://www.nbcnews.com/tech/tech-news/apple-loses-patent-case-university-wisconsin-faces-862m-damages-n443941)