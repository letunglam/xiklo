title: Hơn một nửa tìm kiếm trên Google đến từ điện thoại di động
date: 2015-10-14 11:00:00 GMT+2
cover: /upload/102015-amil-singhal.jpg
featured: true
categories:
- Công nghệ
- Google
tags:
- Google
- Search
---

Nếu bạn hỏi bất kỳ người sử dụng công nghệ về công cụ tốt nhất khi bạn cần phải tìm hiểu điều gì đó, câu trả lời của họ có thể là Google.

Phó chủ tịch cấp cao của Google, Amit Singhal, mới đây công bố rằng Google nhận trên 100 tỷ lượt tìm kiếm mỗi tháng. Đáng kinh ngạc là hơn một nửa các lượt truy cập đến từ thiết bị di động với màn hình nhỏ hơn 6 inch.

<!-- more -->

Với Google là công cụ tốt nhất và bây giờ thêm nhiều tính năng như Google Now thì hàng tỷ lượt tìm kiếm mới chỉ là khởi đầu. Amit cho biết môi trường và thiết bị quyết định cách chúng ta tìm kiếm. Từ cách gõ lệnh cổ điển cho đến ra lệnh bằng giọng nói, Google bây giờ đều đáp ứng hiện tại và cả nhiều năm sau này.

Nguồn: [spilly](http://spilly.co/over-half-of-google-searches-now-come-from-mobile/)