title: Facebook trên iOS liên tục làm cạn kiệt pin
date: 2015-10-15 14:00:00 GMT+2
cover: /upload/102015-facebook-ios.png
categories:
- Công nghệ
tags:
- Facebook
- Apple
- iOS
---

Nhiều người dùng iOS cho biết ứng dụng Facebook sử dụng một số lượng pin lớn bất thường ngay kể cả khi nó chạy ngầm. Và sự việc càng tệ hơn nếu bạn không cho phép Facebook cập nhật ngầm bằng cách tắt Background App Refresh. Bởi Facebook sau đó sẽ tạo ra luồng mới để tiếp tục cập nhật bất chấp người dùng rõ ràng không muốn như vậy.

<!-- more -->

Như Federico Viticci từ **MacStories** đề cập, "thực tế rằng một công ty lớn như Facebook mà không thể tối ưu hóa lượng pin tiêu thụ bởi ứng dụng iOS của họ là một điều rất vô lý. Trường hợp họ có thể nhưng không muốn làm điều đó thì thậm chí còn tệ hơn". Ông đề nghị người dùng iOS không cài đặt ứng dụng Facebook, thay vào đó truy cập Facebook từ Safari. 

> Trên iPhone bạn gái của tôi, iOS 9 báo sử dụng 5 giờ trên màn hình trong 7 ngày qua, và 11 giờ phát âm thanh nền mặc dù đã tắt chế độ cập nhật nền (Background App Refresh). Tôi đoán Facebook đã phát âm thanh không tiếng trên iOS bằng cách cho một Video chạy trong ứng dụng. Thêm vào đó, theo mặc định, video trên Facebook sẽ tự động chơi trên cả hai chế độ Wi-Fi và 3G và rất ít người tắt nó đi, từ đó ứng dụng Facebook sẽ luôn có cách để phát video, giữ âm thanh nền, và tiêu thụ năng lượng để thực hiện tác vụ cập nhật nền".

Liệu Apple có nên xem xét bổ sung việc quản lý pin để chống lại các hành vi luồn lách giống âm thanh nền được sử dụng bởi Facebook trên iOS?

Nguồn: [iphoneincanada](http://www.iphoneincanada.ca/news/facebook-ios-drains-iphone-battery/)