title: Honda là công ty mới nhất được cấp phép thử nghiệm xe không người lái ở California
date: 2015-09-14 12:42:00 GMT+2
categories:
- Công nghệ
tags:
- Honda
- Google
---

Honda đa nhận được giấy phép từ bang California để vận hành xe tự động ở đường phố công cộng tham gia vào lĩnh vực đầy triển vọng này cùng với hàng loạt các công ty như Volkswagen cũng như Google. 

<!-- more -->

Trên website của sở phương tiện cơ giới California đã xác nhận Honda là công ty mới nhất trong số 10 công ty đang tham gia thử nghiệm xe không người lái tại bang này. Trong số các công ty khác cũng tham gia thử nghiệm có Mercedes Benz, Tesla Motors, Nissan Motor và BMW AG.

Honda đang triển khai các hệ thống hỗ trợ điều khiển tiên tiến trên các đời xe Honda và Acura. Công ty cũng là một trong nhiều nhà sản xuất đầu tư vào công nghệ tự lái. Một số nhà sản xuất như Tesla và BMW, đã hỗ trợ tính năng lái bán tự động.

Phát ngôn viên cho biết, hãng ô tô Nhật Bản đã có một cơ sở thử nghiệm xe tự lái tại Concord, California, phía đông bắc của San Francisco, tại đây những chiếc xe tự lái có thể được chạy thử nghiệm trước khi thử nghiệm trên đường. 

California là một trong một số ít các bang, cùng với Michigan, Florida và Nevada, đã thông qua luật cho phép thử nghiệm xe ôtô tự lái xe trên đường công cộng.

Google và các nhà sản xuất và các nhà cung cấp ôtô khác cho biết công nghệ xe tự lái sẽ sẵn sàng vào năm 2020.

Nguồn: [Reuters](http://www.reuters.com/article/2015/09/10/us-honda-autos-self-driving-idUSKCN0RA2G820150910)