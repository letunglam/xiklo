title: Giao diện mới của Xbox One ​​ra mắt thử nghiệm tuần này
date: 2015-09-14 18:42:00 GMT+2
cover: /upload/092015-xbox-windows-10-ui.jpg
categories:
- Công nghệ
- Microsoft
tags:
- Microsoft
- Xbox One
---

Xbox One sẽ có giao diện hoàn toàn mới trong mùa thu này. Với giao diện mới này mọi thao tác sẽ nhanh hơn 50% so với hiện tại ấn tượng ban đầu rất tốt khi Microsoft trình diễn.

<!-- more -->

Microsoft không công bố ngày cụ thể sẽ ra mắt toàn bộ người dùng. Tuy nhiên, theo Phil Spencer, thành viên chương trình thử nghiệm (Preview) sẽ được tiếp cận giao diện mới trong tháng này.

Trong hôm nay, người phát ngôn của Microsoft đã cho biết thông tin rằng người dùng thử nghiệm của Xbox One sẽ nhận được lời mời sử dụng giao diện mới muộn nhất là trong tuần này. 

![Xbox One New UI Invite](/upload/092015-xbox-one-new-ui-notice.png)

Tuy nhiên, không phải tất cả thành viên thử nghiệm sẽ được tiếp cận giao diện mới cùng một lúc. Microsoft sẽ mời những thành viên có lượng đóng góp ý kiến và năng nổ nhất trước, sau đó mới đến những thành viên còn lại.

Bạn có thể xem qua trình diễn giao diện mới của Xbox One ở Video dưới đây

<div class="embed">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/JQNIyJhdIbk?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>