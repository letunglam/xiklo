title: Phụ kiện chống rung khi quay phim cho hầu hết các thiết bị.  
date: 2015-09-14 13:42:00 GMT+2
cover: /upload/092015-SteadXP.jpg
categories:
- Công nghệ
tags:
- SteadXP
---

Không phải tất cả máy ảnh đều có tính năng chống rung. Điều này có nghĩa là đoạn phim được quay sẽ bị rung lắc, trừ khi bạn có khả năng giữ tay cố định. Có nhiều lựa chọn để giải quyết vấn đề này như chân máy chống rung, hay đế thăng bằng. Tuy nhiên, nếu những lựa chọn trên không làm bạn hài lòng, bạn có thể sẽ quan tâm đến một dự án trên Kickstarter có tên gọi SteadXP. 

<!-- more -->

Bạn có thể thắc mắc SteadXP là gì? Về cơ bản thì đây là một phụ kiện mà bạn có thể gắn vào máy ảnh của bạn. Nó sẽ có thể để ghi lại chuyển động của máy ảnh để nó biết khi có di chuyển. Điều này là nhờ vào bộ vi xử lý trong 32-bit ARM, 3-trục con quay hồi chuyển và một gia tốc 3-trục.

Bạn đưa các đoạn phim sau khi quay vào máy tính. Vì nó nhớ chuyển động của bạn khi quay, nên thông qua phần mềm chống rung nó có thể bù đắp rung lắc xảy ra trong quá trình quay giúp cho đoạn phim bớt rung hơn. Đây thực sự là một ý tưởng tốt. 

Nhược điểm duy nhất là bạn không biết đoạn phim cuối cùng sẽ như thế nào cho đến khi bạn đã xử lí thông qua phần mềm. SteadXP tương thích với GoPro và DSLR/máy ảnh không gương lật. Giá công bố tương ứng là **$159** và **$261**.

Nguồn: [ubergizmo](http://www.ubergizmo.com/2015/09/steadxp-camera-stabilizer-kickstarter/)