title: Nút "Không thích" sẽ không xuất hiện trên Facebook
date: 2015-09-16 02:00:00 GMT+2
cover: /upload/092015-mark.jpg
categories:
- Công nghệ
- Facebook
tags:
- Facebook
---

Giám đốc điều hành Facebook - Mark Zuckerberg - vốn không thích ý tưởng của nút "không thích" (dislike), nhưng ông cho biết rằng Facebook sẽ sớm được thử nghiệm một giải pháp thay thế cho các nút "thích" (like), một cách khác để mọi người bày tỏ cảm xúc ngắn gọn trên các mạng xã hội.

<!-- more -->

*"Tôi nghĩ mọi người đã yêu cầu nút không thích trong nhiều năm. Hàng trăm người có lẽ cũng đã thắc mắc về điều này "*, Zuckerberg nói trong một phiên hỏi đáp ở Menlo Park trụ sở chính của công ty. *"Hôm nay là một ngày đặc biệt vì hôm nay là ngày mà tôi thực sự có thể nói rằng chúng tôi đang việc đó và sắp đưa ra thử nghiệm."*

Câu nói của Zuckerberg dẫn đến nhiều tiêu đề trên các bào về sự xuất hiện của nút "không thích", nhưng điều đó có thể sẽ không xảy ra.

Quan điểm thường được tuyên bố của Zuckerberg là ông không muốn tạo ra một tình huống mọi người có thể bác (downvoting) bài viết của người khác. Zuckerberg không đưa chi tiết cụ thể hoặc nút đó sẽ được gọi là gì nhưng cho biết rằng ý tưởng này sẽ là một cách để thể hiện sự đồng cảm.

*"Không phải mọi khoảnh khắc đều tốt đẹp và nếu bạn đang chia sẻ một điều gì đó đáng buồn, như cuộc khủng hoảng người tị nạn khiến bạn xúc động, hoặc một thành viên trong gia đình qua đời, sẽ không thoải mái để thích bài đó,"* ông nói. *"Nhưng bạn bè của bạn và mọi người muốn thể hiện rằng họ hiểu và họ đồng cảm với bạn. Vì vậy, tôi nghĩ rằng điều quan trọng là để cho mọi người nhiều cách để bày tỏ và chia sẻ về những cảm nhận của họ thay vì chỉ đơn thuần là thích như hiện tại."*

*"Chúng tôi đã làm việc này được một thời gian. Nó phức tạp một cách đang ngạc khiên để làm cho tương tác đơn giản như bạn muốn. Chúng tôi có một ý tưởng và sẽ sớm đưa ra thử nghiệm. Và tùy thuộc vào nó như thế nào, chúng tôi sẽ phổ biến nó rộng rãi hơn."*

Bạn có thể xem bài phát biểu của Zuckerberg về giải pháp thay thế cho nút "thích"

<div class="embed">
  <iframe src="https://player.vimeo.com/video/139401042" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

Nguồn: [Marketingland](http://marketingland.com/zuckerberg-facebook-is-close-to-testing-an-other-than-like-button-142663)