title: Facebook thử nghiệm sáu cảm xúc thay vì một nút duy nhất "thích"
date: 2015-10-10 01:00:00 GMT+2
cover: /upload/102015-facebook-new-emoji.png
featured: true
categories:
- Công nghệ
- Facebook
tags:
- Facebook
---

Như đã từng đưa tin, Mark Zuckerberg đã gợi ý về một nút mới thay cho nút "thích" như hiện tại. Rõ ràng là trên mạng xã hội Facebook có rất nhiều thứ bạn không muốn "thích" một chút nào. Giờ đây, bạn sẽ được thoả lòng sớm, bởi Facebook đang thử nghiệm các cách thể hiện cảm xúc mới. Khá đơn giản, thay vì chỉ có "thích" như hiện tại, bạn có 7 cách nữa để bày tỏ cảm xúc: thích, yêu, cười, yay, tuyệt, buồn và tức giận.

<!-- more -->

<div class="embed">
  <iframe name="f1b652ada4" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" src="http://www.facebook.com/v2.3/plugins/video.php?allowfullscreen=true&amp;app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FYKPvjVoWVGb.js%3Fversion%3D41%23cb%3Df35af96f7%26domain%3Dgizmodo.com%26origin%3Dhttp%253A%252F%252Fgizmodo.com%252Ff1757bfac%26relation%3Dparent.parent&amp;container_width=636&amp;href=https%3A%2F%2Fwww.facebook.com%2Fvideo.php%3Fv%3D10102413019800771&amp;locale=en_US&amp;sdk=joey"></iframe>
</div>

Nhưng bạn cũng không nên quá nóng ruột, hiện tại các tính năng mới chỉ được thử nghiệm ở Ireland và Tây Ban Nha. Tại đây, việc thể hiện cảm xúc như nêu ở trên sẽ được điều chỉnh dựa trên việc người dùng cảm thấy như thế nào về tính năng này. Và các dữ liệu này sẽ được Facebook và các nhãn hàng, công ty trên Facebook thu thập lại. TechCrunch có nêu:

Các dữ liệu thu thập được sẽ được đưa đến trang thống kê cho chủ nhân của các trang trên Facebook. Như vậy hiện nay, các nhà quản lý sẽ có thể biết thêm về cảm xúc của khách hàng/người xem chứ không chỉ đơn thuần là chia sẻ hay thích như trước đây. Thêm vào đó, những dữ liệu này cũng sẽ đóng vai trò lớn với việc quảng cáo của Facebook, bởi từ đó, họ có thể cung cấp quảng cáo hoặc thông tin thích hợp hơn với nhu cầu của người xem.

Nguồn: [TechCrunch](http://techcrunch.com/2015/10/08/with-reactions-facebook-supercharges-the-like-button-with-6-empathetic-emoji/)