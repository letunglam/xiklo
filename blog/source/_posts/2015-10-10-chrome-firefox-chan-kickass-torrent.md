title: Chrome và Firefox chặn trang Torrent Kickass
date: 2015-10-10 03:00:00 GMT+2
categories:
- Công nghệ
tags:
- Torrent
- Kickass
- Chrome
- Firefox
---

Một vài giờ gần đây người sử dụng Chrome và Firefox không thể truy cập vào trang Torrent Kickass, một trong những website chia sẻ Torrent lớn nhất thế giới. Thay vào website Kickass như mọi khi, bây giờ người dùng nhận được thông tin cảnh báo từ trình duyệt như dưới đây:

<!-- more -->

![Thông báo chặn của Chrome](/upload/102015-kickass.png)

Thông điệp cảnh báo được đưa ra bởi chương trình quét phần mềm không mong muốn (Unwanted Software) của Google, chương trình sẽ thông báo nếu có nguy hiểm tiềm năng đến từ một website nào đó cho khách truy cập. Cả Chrome và Firefox đều sử dụng dịch vụ này để tránh người dùng không bị nhiễm phần mềm độc hại. Chính sách này áp dụng cho toàn bộ website, nhưng riêng những trang chia sẻ Torrent thường xuyên là mục tiêu của việc phát tát phần mềm độc hại, theo Google.

Đây không phải lần đầu tiên website này bị chặn bởi Google, cách đây vài tháng Google cũng đã tiến hành chặn website này cùng với cảnh báo như trên. Và những website chia sẻ torrent lớn khác cũng bị ảnh hưởng. 

Theo nhóm điều hành website Kickass thì việc bị chặn là do quảng cáo trên website của họ có chứa mã độc, và họ đã tiến hành bỏ quảng cáo đó ra khỏi website. Họ cũng cho biết cảnh báo của Google sẽ được gỡ bỏ sớm. 

Nguồn: [TorrentFreak](https://torrentfreak.com/chrome-and-firefox-block-kickasstorrents-over-harmful-programs-151009/)