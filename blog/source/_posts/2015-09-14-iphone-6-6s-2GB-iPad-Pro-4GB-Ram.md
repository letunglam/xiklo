title: Xcode xác nhận iPhone 6 và 6s có 2GB Ram, iPad Pro có 4GB Ram
date: 2015-09-14 11:42:00 GMT+2
categories:
- Công nghệ
- Apple
tags:
- Xcode
- Apple
- iPhone
- iPad
---

Hamza Sood - một nhà phát triển ứng dụng iOS - hôm nay khám phá trên Xcode, một công cụ giúp lập trình phần mềm cho iOS, có chứa thông tin về RAM của iPhone 6, 6s và iPad Pro. Theo thông tin đưa ra bởi Xcode thì iPhone 6 và 6s có 2GB Ram và iPad Pro có 4GB Ram

<!-- more -->

![](/upload/092015-iphone-ipad-ram.jpg)

Các tin đồn về Ram iPhone 6 và 6s đều cho thấy thiết bị sẽ có 2GB Ram. Cùng thời điểm, Adobe cũng đưa ra thông cáo báo chí, trong đó có nêu iPad Pro có 4GB Ram nhưng sau đó bị gỡ bỏ. 

Như thường lệ, Apple không nêu cấu hình cụ thể của thiết bị sắp được ra mắt trong các sự kiện của mình, và trường hợp iPhone 6, 6s, iPad Pro cũng không ngoại lệ. 

Nguồn: [MacRumors](http://www.macrumors.com/2015/09/13/xcode-iphone-6s-2gb-ram-ipad-pro-4gb-ram/)