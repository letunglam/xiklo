title: Lượng đơn đặt hàng iPhone 6s và 6s Plus đang trên đà phá kỉ lục của năm ngoái
date: 2015-09-14 22:42:00 GMT+2
categories:
- Công nghệ
- Apple
tags:
- Apple
- iPhone
---

Apple đã ra thông cáo báo chí rằng lượng đặt hàng trước iPhone 6s và 6s Plus đang trên đà phá kỉ lục 10 triệu iPhone 6 và iPhone 6 Plus của tuần ra mắt tháng 9 năm ngoái. 

<!-- more -->

iPhone 6s và iPhone 6s có thể đặt hàng trước bắt đầu vào ngày thứ Bảy. Trong khi hầu hết các model iPhone 6s Plus và Rose Gold hiện nay chỉ được xuất xưởng trong 2-3 tuần sau khi đặt hàng thì các model iPhone 6s khác có sẵn để giao hàng vào ngày 25 tháng 9.

> iPhone 6s và iPhone 6s Plus đã được khách hàng phản hồi vô cùng tích cực và lượng đơn đặt hàng cuối tuần này có số lượng rất lớn trên toàn thế giới. Chúng tôi đang trên đà để phá kỉ lục 10.000.000 iPhone được bán ra trong tuần đầu tiên ra mắt vào 25 tháng 9.

> Như nhiều khách hàng nhận thấy, nhu cầu cho iPhone 6s Plus đặc biệt lớn và vượt dự báo của chúng tôi trong giai đoạn đặt hàng trước. Chúng tôi đang cố gắng hết sức có thể để theo kịp tiến độ một cách nhanh nhất, và iPhone  6s cũng như iPhone 6s Plus sẽ có mặt tại các cửa hàng bán lẻ Apple khi chúng được mở cửa vào thứ sáu tới.

> --- theo Apple

Năm ngoái, Apple công bố iPhone 6 và iPhone 6 Plus đạt kỷ lục 4 triệu đơn đặt hàng trước chỉ tính riêng ngày đầu tiên ra mắt, và xác nhận con số bán ra vào cuối tuần đạt 10 triệu đơn. Một tháng sau đó, báo cáo cho thấy iPhone 6 và iPhone 6 Plus đạt 20 triệu đơn tại Trung Quốc thông nhà mạng và trang web thương mại điện tử Jingdong Mall.

Nguồn: [MacRumors](http://www.macrumors.com/2015/09/14/iphone-6s-preorders-10-million-first-weekend/)