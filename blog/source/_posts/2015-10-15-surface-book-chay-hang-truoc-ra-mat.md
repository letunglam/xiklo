title: Surface Book cháy hàng trước khi ra mắt
date: 2015-10-15 13:00:00 GMT+2
cover: /upload/102015-surface-book.png
categories:
- Công nghệ
- Microsoft
tags:
- Microsoft
- Surface Book
---

Những người muốn mua Surface Book của Microsoft, laptop lai máy tính bảng, sẽ phải tìm một cửa hàng khác để mua bởi cửa hàng bán lẻ trực tuyến của Microsoft online Store hiện không cho người dùng đặt mua, thay vào đó chỉ cho phép họ để lại email nhận không báo khi có hàng trở lại.

Đối với Microsoft, điều này có nghĩa là nhu cầu với hệ máy này rất lớn. Và đó là một dấu hiệu tích cực với Surface Book và cho thấy chiến lược tiến vào thị trường của Laptop của Microsoft đã khác rất nhiều so với trước đây hãng chỉ đứng ngoài.

<!-- more -->

Surface Book được công bố cuối tuần trước trong một sự kiện báo chí Microsoft ở New York. Máy có một màn hình cảm ứng độ phân giải cao và một bàn phím có thể kết nối với màn hình thông qua một bản lề đặc biệt. Bản lề cho phép tách phần trên máy tính xách tay để sử dụng nó như một máy tính bảng. Người dùng cũng có thể mua cấu hình mạnh hơn với giá cao hơn để nâng cấp chip xử lý đồ họa.

Những người muốn đặt hàng trực tuyến vẫn có thể làm được điều đó thông qua các đối tác bán lẻ của Microsoft như Amazon, Best Buy và Fry's Electronics. Điều này có nghĩa là bạn vẫn có một chút cơ hội sở hữu Surface Book vào ngày 29 tháng 11 này, chỉ không rõ là khi nào Microsoft sẽ có đợt hàng tiếp theo.

Nên nhớ, Microsoft Band đời đầu mặc dù nó đã được bán ra do "nhu cầu lớn hơn dự kiến". Nó vẫn không thúc đẩy mạnh mẽ thị trường thiết bị đeo tay. Tuy nhiên, cháy hàng trước ngày ra mắt là bước khởi đầu rất tốt.

Nguồn: [pcworld](http://www.pcworld.com/article/2991522/laptop-computers/surface-book-pre-orders-sell-out-on-microsofts-site.html)