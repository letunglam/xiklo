var gulp              = require('gulp'),
    sass              = require('gulp-sass'),
    concat            = require('gulp-concat'),
    autoprefixer      = require('gulp-autoprefixer'),
    imagemin          = require('gulp-imagemin'),
    pngquant          = require('imagemin-pngquant'),
    shell             = require('gulp-shell'),
    path              = require('path'),
    uglify            = require('gulp-uglify'),
    version           = require('gulp-version-number'),
    awspublish        = require('gulp-awspublish'),
    awspublishRouter  = require("gulp-awspublish-router");



gulp.task('css', function() {
  return gulp.src('themes/xiklo/source/assets/sass/*.scss')
    .pipe( 
      sass( { 
        includePaths: ['themes/xiklo/source/assets/sass'],
        errLogToConsole: true,
        outputStyle: 'compressed'
      } ) )
    .on('error', onError)
    .pipe( autoprefixer() )
    .on('error', onError)
    .pipe( gulp.dest('themes/xiklo/source/assets/stylesheets/') )
});

// --- Optimize Images ---
gulp.task('optimg', function () {
  return gulp.src('source/upload/*')
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('source/upload/'));
});

// --- Minify JS ---
gulp.task('jscompress', function() {
  return gulp.src([
      'themes/xiklo/source/js/jquery.min.js',
      'themes/xiklo/source/js/jquery.timeago.js',
      'themes/xiklo/source/js/script.js'
    ])
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('themes/xiklo/source/assets/javascripts/'));
});

// --- Hexo ---
gulp.task('hexo', shell.task([
  'hexo server'
]));
gulp.task('hexogen', shell.task([
  'hexo generate'
]));

// --- UnCSS ---

// --- Hexo ---
gulp.task('watch', function () {
  gulp.watch('themes/xiklo/source/assets/js/*.js',['jscompress']);
  gulp.watch('themes/xiklo/source/assets/sass/*.scss',['css']);
});

// Publish to Amazon S3 / CloudFront
gulp.task("publish", function () {
  var publisher = awspublish.create({
    params: { 
      "Bucket": 'xiklo.com'    
    },
    "accessKeyId": 'AKIAJ7L2RJH5UCIVPRNQ',
    "secretAccessKey": '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',
  });

  gulp.src(['**/*', '!js/*', '!assets/sass/*'], { cwd: "./public/" })
    .pipe(awspublishRouter({
      cache: {
        cacheTime: 300
      },
      routes: {
        "^assets/(?:.+)\\.(?:js|css)$": {
          key: "$&",
          gzip: true, // use gzip for assets that benefit from it
          cacheTime: 630720000, // cache static assets for 20 years
          headers: {
            "Content-Encoding": "gzip",
          }          
        },
        "^assets/.+$": {
          cacheTime: 630720000 // cache static assets for 20 years
        },
        "^.+\\.html": {
          gzip: true,
          // cacheTime: 21600, // cache static assets for 6 hours
          headers: {
            "Content-Encoding": "gzip"
          }
        },
        "^README$": {
          // specify extra headers
          headers: {
            "Content-Type": "text/plain"
          }
        },
        // pass-through for anything that wasn't matched by routes above, to be uploaded with default options
        "^.+$": "$&"
      }
    }))
    .pipe(publisher.publish())
    .pipe(publisher.sync())
    .pipe(awspublish.reporter())
});

function onError(err) {
  console.log(err);
  this.emit('end');
}

// Default Task
gulp.task('default', ['css','jscompress','hexo','watch']);
gulp.task('pre', ['css','optimg','jscompress','hexogen']);